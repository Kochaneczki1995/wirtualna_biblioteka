<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
/*<tr>
                                <td>{{ $history->title }}</td>
                                <td>{{ $history->author }}</td>
                                <td>{{ $history->created_at }}</td>
                                <td>{{ $history->updated_at }}</td>
                            </tr>*/

class User extends Authenticatable
{
    public function books()
    {
        return $this->belongsToMany('App\Book')
            ->withPivot('status')
            ->withTimestamps();

        return $this->belongsToMany('App\Book', 'book_user')
            ->withTimestamps();
    }

    public function history()
    {
        return $this->hasMany('App\History')
            ->withTimestamps();
    }
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isAdmin() {
        return $this->admin;    // sprawdza kolumnę 'admin' w tabeli 'users'
    }
}
