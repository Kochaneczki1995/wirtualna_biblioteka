<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use Auth;
use App\User;



class BasketController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        echo 'index';
        //return redirect()->route('catalog.index');
        //return view('basket.index');
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
    }

    public function show($id)
    {
        //$books=Book::where('available',1);
        //$users=User::all()

    }

    public function edit($id)
    {

        $users = User::all();
        $books = Book::all();
        $book = $books->where('id', $id)->first();
        $user_id = Auth::user()->id;
        $user=User::where('id',$user_id)->first();

        if ($book->available != 0){
            $book->users()->attach($user_id);
            $book->available = $book->available - 1;
            $book->save();
            return view('basket.edit')->withBook($book);//->withPivot('status');
        }

    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
