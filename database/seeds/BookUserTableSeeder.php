<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Book;

class BookUserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('book_user')->delete();

        $users = User::all();
       $books = Book::all();

        foreach ($books as $book)
        {
            foreach ($users as $user)
            {
                $book->users()->attach($user);
            }
        }
    }
}
