<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();

        User::create([
            'name' => 'root',
            'email' => 'root@ro.ot',
            'password' => Hash::make('123'),
            'admin' => True
        ]);

        User::create([
            'name' => 'qwerty',
            'email' => 'qw@er.ty',
            'password' => Hash::make('456'),
            'borrowed' => 1,
        ]);

        User::create([
            'name' => 'Anna',
            'email' => 'an@example.com',
            'password' => Hash::make('aaa111'),
            'borrowed'=>2,
        ]);

        User::create([
            'name' => 'Grazyna',
            'email' => 'graga@example.com',
            'password' => Hash::make('aaa111'),
            'borrowed'=>1,
        ]);

        User::create([
            'name' => 'Teofil',
            'email' => 'teo@example.com',
            'password' => Hash::make('aaa111'),
        ]);
    }
}
