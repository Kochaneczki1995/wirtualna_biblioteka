@extends('layouts.app')

<div class="container">
  <div class="row">
    <div class="col-md-10 col-md-offset-1">
      <div class="panel panel-default">

        <div class="panel-heading">Książka została umieszczona w koszyku</div>

        <div class="panel-body">
          <table class='table table-hover'>

            <tr>
              <th>TYTUŁ</th>
              <th>AUTOR</th>
            </tr>
            <tr>
              <td>{{$book->title}}</td>
              <td>{{$book->author}}</td>
            </tr>
          </table>
          <a href="{{ url('/catalog')}}">Powrót do katalogu</a><br>
          <a href="{{ url('/user/borrowed')}}">Moje wypożyczenia</a>

        </div>
        <div>

        </div>
      </div>


