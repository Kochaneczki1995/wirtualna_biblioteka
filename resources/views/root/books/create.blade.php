@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">Nowa książka</div>

                    <div class="panel-body">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" action="{{route('books.store')}}" method="post">

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Tytuł</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="title" value="{{ old('title') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="author" class="col-md-4 control-label">Autor</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="author" value="{{ old('author') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="ISBN" class="col-md-4 control-label">ISBN</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="ISBN" value="{{ old('ISBN') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="edition" class="col-md-4 control-label">Wydanie</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="edition" value="{{ old('edition') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="publication" class="col-md-4 control-label">Wydawnictwo</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="publication" value="{{ old('publication') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="copies" class="col-md-4 control-label">Liczba kopii</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="copies" value="{{ old('copies') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <input type="submit" value="Dodaj książkę" class="btn btn-primary">
                                    {{csrf_field()}}
                                </div>
                            </div>

                        </form>
                        <br><a href="{{ route('books.index') }}">Powrót</a>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection