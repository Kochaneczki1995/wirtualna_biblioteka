@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">Nowy użytkownik</div>

                    <div class="panel-body">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" action="{{route('users.store')}}" method="post">

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label">Nazwa</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="name" value="{{ old('name') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">E-mail</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="email" value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="Password" class="col-md-4 control-label">Hasło</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" name="password" value="{{ old('password') }}">
                                </div>
                            </div>

                            {{--<label for="admin">Admin</label>--}}
                            {{--<input type="text" name="admin" value="{{ old('admin') }}">--}}

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <input type="submit" value="Dodaj użytkownika" class="btn btn-primary">
                                    {{csrf_field()}}
                                </div>
                            </div>
                        </form>
                        <br><a href="{{ route('users.index') }}">Powrót</a>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection