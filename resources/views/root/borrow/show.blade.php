@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-heading">Książki wypożyczone przez użytkownika {{ $user->name }}</div>

                    <div class="panel-body">
                        <table class='table table-hover'>
                            <th>Tytuł</th><th>Autor</th><th>ISBN</th><th>Edycja</th><th>Publikacja</th><th>Zmień status</th>
                            @foreach($user->books as $bookBasket)
                                @if($bookBasket->pivot->status==1)
                                    <tr>
                                        <td>{{ $bookBasket->title }}</td>
                                        <td>{{ $bookBasket->author }}</td>
                                        <td>{{ $bookBasket->ISBN }}</td>
                                        <td>{{ $bookBasket->edition }}</td>
                                        <td>{{ $bookBasket->publication }}</td>
                                        <td><a href="/admin/borrow/{{ $user->id }}/edit/{{ $bookBasket->id }}">Zwrot</a></td>
                                    </tr>
                                @endif
                            @endforeach
                        </table>
                    </div>
                </div>

                <div class="panel panel-default">

                    <div class="panel-heading">Książki w koszyku użytkownika {{ $user->name }}</div>

                    <div class="panel-body">
                        <table class='table table-hover'>
                            <th>Tytuł</th><th>Autor</th><th>ISBN</th><th>Edycja</th><th>Publikacja</th><th>Zmień status</th>
                            @foreach($user->books as $bookBasket)
                                @if($bookBasket->pivot->status==0)
                                <tr>
                                    <td>{{ $bookBasket->title }}</td>
                                    <td>{{ $bookBasket->author }}</td>
                                    <td>{{ $bookBasket->ISBN }}</td>
                                    <td>{{ $bookBasket->edition }}</td>
                                    <td>{{ $bookBasket->publication }}</td>
                                    <td><a href="/admin/borrow/{{ $user->id }}/edit/{{ $bookBasket->id }}">Wypożycz</a></td>
                                </tr>
                                @endif
                            @endforeach

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection