@extends('layouts.app')

@section('content')

    <form action="/search" method="POST" class="form-horizontal">

        <div class="form-group">
            <div class="col-md-4 col-md-offset-4">
        &nbsp;&nbsp;&nbsp;&nbsp;    <input type="text" name="search" value="" placeholder="  Szukaj..." class="form-control">
                <input type="hidden" name="_token" value="{{csrf_token()}}" >
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-4 col-md-offset-4">
                <input type="submit" value="Szukaj" class="btn btn-primary">
            </div>
        </div>

    </form>
    
    <br>

    <table class="table table-hover">
        <tr>
            <th>TYTUŁ</th>
            <th>AUTOR</th>
            <th>ISBN</th>
            <th>EDYCJA</th>
            <th>PUBLIKACJA</th>
            <th>ILOŚĆ</th>
            <th>DOSTĘPNYCH</th>
            <th>OPCJE</th>
        </tr>

        @foreach( $catalogs as $catalog)
            <tr>
                <td>{{$catalog->title}}</td>
                <td>{{$catalog->author}}</td>
                <td>{{$catalog->ISBN}}</td>
                <td>{{$catalog->edition}}</td>
                <td>{{$catalog->publication}}</td>
                <td>{{$catalog->copies}}</td>
                <td>{{$catalog->available}}</td>

                @if($catalog->available == 0)
                    <td>Brak</td>
                @else
                    <td><a href="{{ route('basket.edit',$catalog->id) }}">Wypożycz</a></td>
                @endif

            </tr>

        @endforeach


    </table>

    {{$catalogs->links() }}

@endsection
