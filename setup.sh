#!/bin/bash
composer install
chmod -R 0777 storage
php artisan cache:clear
composer dump-autoload
php artisan migrate
php artisan db:seed